Purge unused families and types using the Revit API's PerformanceAdviser.

(This solution is intended for Revit 2017+, as earlier versions of Revit throw an InternalException.)